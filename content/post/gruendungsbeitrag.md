+++
title = "Gründungsbeitrag"
author = ["Gregor Goldbach"]
date = 2018-09-30T20:07:00+02:00
draft = false
creator = "Emacs 26.1 (Org mode 9.1.9 + ox-hugo)"
+++

Moin! Ich bin Gregor und werde in diesem Blog meine Reise mit Emacs
dokumentieren.

Dieser Ansatz ist ähnlich wie der von [Emacscast](https://emacscast.org/). Bei mir sind die
Voraussetzungen jedoch etwas anders: Ich kenne den Emacs schon seit etwa 25
Jahren und habe ihn immer wieder unterschiedlich stark genutzt. Ein richtiger
Anfänger bin ich also nicht, anderseits **fühle** ich mich in vielen Bereichen des
Emacs wie einer.

Als ich damals auf meinem Amiga mit dem Emacs anfing, gab es noch kein Reddit
oder Stackoverflow. Wenn ich es mir genau überlege, gab es noch nicht einmal das
World Wide Web. Mein Wissen über den Emacs habe ich damals aus Newsgroups,
Magazinen und selten aus Schnipseln von Disketten mit Public-Domain-Software
gezogen. Wirklich verstanden habe ich nie, was ich da tat.

Das ist jetzt anders. Ich möchte endlich verstehen, was unter der Haube passiert
und was ich da mache. Emacs ist ein Werkzeug in meinem Werkzeugkoffer und ich
möchte ihn gut beherrschen.

Über die Jahre habe ich eine Vielzahl von Schreibwerkzeugen
genutzt: Texteditoren, Textverarbeitungen und Blogging-Werkzeuge. Zwischendurch
kam ich immer wieder zum Emacs zurück und ging wieder. Emacs hat mich privat und
beruflich immer wieder begleitet, auch wenn wir unsere Reise nie konsequent
geplant gemeinsam durchgezogen haben.

Ich bin jetzt wieder beim Emacs. Und ich fühle mich heimisch. Ein anderes Wort
fällt mir dafür nicht ein. Ich weiß, wo alles liegt. Ich kann ohne nachzudenken
etwas machen. Ja, es sieht hier manchmal etwas schräg aus, und die Neubauten da
drüben glitzern so schön und sehen gut aus, aber es ist mein zu Hause. Ich
richte es mir so ein, wie es mir gefällt.

Was möchte ich in diesem Blog machen? Ich schreibe hier auf, was ich mit dem
Emacs mache und vor allem **wie** ich es mache. Das hat zweierlei Wirkung: Ich kann
es für mich nachschlagen und anderen hilft es vielleicht.

Im nächsten Beitrag schreibe ich vermutlich über meine Konfiguration und die von
mir verwendeten Pakete.

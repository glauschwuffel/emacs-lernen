+++
title = "Datenschutzerklärung"
author = ["Gregor Goldbach"]
date = 2018-09-29T09:17:09+02:00
draft = false
+++

Moin!

Auf dieser Seite versuche ich dich in klar verständlichen Worten so zu
informieren, wie es die Datenschutzgrundverordnung von mir verlangt. Dabei lässt
es sich leider nicht vermeiden, dass ich einige Formulierungen wähle, die etwas
sperrig sind und sich nicht so gut lesen lassen. Aber da müssen wir durch.

Der Text auf dieser Seite wurde ursprünglich mit dem [Datenschutz-Generator.de von RA Dr. Thomas
Schwenke](https://datenschutz-generator.de) erstellt und dann von mir angepasst.

Diese Datenschutzerklärung klärt dich darüber auf, welche personenbezogenen Daten auf dieser
Webseite verarbeitet werden und wofür. Damit der Text lesbarer wird, werde ich
im Folgenden nur noch von "Daten" schreiben.

Im Hinblick auf die verwendeten Begrifflichkeiten, wie zum Beispiel "Verarbeitung"
oder "Verantwortlicher" verweise ich auf die Definitionen im [Artikel 4 der
Datenschutzgrundverordnung (DSGVO)](https://dsgvo-gesetz.de/art-4-dsgvo/).

# Verantwortlicher

Gregor Goldbach<br>
Grüner Weg 10<br>
21423 Winsen (Luhe)<br>

E-Mail-Adresse: glauschwuffel@gmail.com

# Arten der verarbeiteten Daten

- Nutzungsdaten (z.B., besuchte Webseiten, Interesse an Inhalten,
Zugriffszeiten).
- Meta-/Kommunikationsdaten (z.B., Geräte-Informationen, IP-Adressen).

# Kategorien betroffener Personen

Besucher und Nutzer dieser Webseite. Nachfolgend bezeichne ich die
betroffenen Personen zusammenfassend auch als „Nutzer“.

# Zweck der Verarbeitung

- Zurverfügungstellung des Onlineangebotes, seiner Funktionen und
Inhalte.
- Beantwortung von Kontaktanfragen und Kommunikation mit Nutzern.
- Sicherheitsmaßnahmen.
- Reichweitenmessung/Marketing


# Maßgebliche Rechtsgrundlagen

Nach Maßgabe des [Artikel 13 DSGVO](https://dsgvo-gesetz.de/art-13-dsgvo/) teile
ich dir die Rechtsgrundlagen meiner Datenverarbeitungen mit. Für Nutzer aus der
EU und des EWG, gilt, sofern die Rechtsgrundlage in der Datenschutzerklärung
nicht genannt wird, Folgendes.

- Die Rechtsgrundlage für die Einholung von Einwilligungen ist Art. 6 Abs.
1 lit. a und Art. 7 DSGVO.
- Die Rechtsgrundlage für die Verarbeitung zur Erfüllung meiner
Leistungen und Durchführung vertraglicher Maßnahmen sowie Beantwortung
von Anfragen ist Art. 6 Abs. 1 lit. b DSGVO.
- Die Rechtsgrundlage für die Verarbeitung zur Erfüllung meiner
rechtlichen Verpflichtungen ist Art. 6 Abs. 1 lit. c DSGVO.
- Für den Fall, dass lebenswichtige Interessen der betroffenen Person oder
einer anderen natürlichen Person eine Verarbeitung personenbezogener
Daten erforderlich machen, dient Art. 6 Abs. 1 lit. d DSGVO als
Rechtsgrundlage.
- Die Rechtsgrundlage für die erforderliche Verarbeitung zur Wahrnehmung
einer Aufgabe, die im öffentlichen Interesse liegt oder in Ausübung
öffentlicher Gewalt erfolgt, die dem Verantwortlichen übertragen wurde
ist Art. 6 Abs. 1 lit. e DSGVO.
- Die Rechtsgrundlage für die Verarbeitung zur Wahrung meiner
berechtigten Interessen ist Art. 6 Abs. 1 lit. f DSGVO.
- Die Verarbeitung von Daten zu anderen Zwecken als denen, zu denen sie
ehoben wurden, bestimmt sich nach den Vorgaben des Art 6 Abs. 4 DSGVO.
- Die Verarbeitung von besonderen Kategorien von Daten (entsprechend Art.
9 Abs. 1 DSGVO) bestimmt sich nach den Vorgaben des Art. 9 Abs. 2
DSGVO.

# Sicherheitsmaßnahmen

Ich treffe nach Maßgabe der gesetzlichen Vorgaben unter Berücksichtigung des
Stands der Technik, der Implementierungskosten und der Art, des Umfangs, der
Umstände und der Zwecke der Verarbeitung sowie der unterschiedlichen
Eintrittswahrscheinlichkeit und Schwere des Risikos für die Rechte und
Freiheiten natürlicher Personen, geeignete technische und organisatorische
Maßnahmen, um ein dem Risiko angemessenes Schutzniveau zu gewährleisten.

Zu den Maßnahmen gehören insbesondere die Sicherung der Vertraulichkeit,
Integrität und Verfügbarkeit von Daten durch Kontrolle des physischen
Zugangs zu den Daten, als auch des sie betreffenden Zugriffs, der
Eingabe, Weitergabe, der Sicherung der Verfügbarkeit und ihrer Trennung.
Des Weiteren habe ich Verfahren eingerichtet, die eine Wahrnehmung von
Betroffenenrechten, Löschung von Daten und Reaktion auf Gefährdung der
Daten gewährleisten. Ferner berücksichtige ich den Schutz
personenbezogener Daten bereits bei der Entwicklung, bzw. Auswahl von
Hardware, Software sowie Verfahren, entsprechend dem Prinzip des
Datenschutzes durch Technikgestaltung und durch datenschutzfreundliche
Voreinstellungen.

# Zusammenarbeit mit Auftragsverarbeitern, gemeinsam Verantwortlichen und Dritten

Sofern ich im Rahmen meiner Verarbeitung Daten gegenüber anderen
Personen und Unternehmen (Auftragsverarbeitern, gemeinsam
Verantwortlichen oder Dritten) offenbaren, sie an diese übermitteln oder
ihnen sonst Zugriff auf die Daten gewähren, erfolgt dies nur auf
Grundlage einer gesetzlichen Erlaubnis (z.B. wenn eine Übermittlung der
Daten an Dritte, wie an Zahlungsdienstleister, zur Vertragserfüllung
erforderlich ist), Nutzer eingewilligt haben, eine rechtliche
Verpflichtung dies vorsieht oder auf Grundlage meiner berechtigten
Interessen (z.B. beim Einsatz von Beauftragten, Webhostern, etc.).

# Übermittlungen in Drittländer

Sofern ich Daten in einem Drittland (d.h. außerhalb der Europäischen Union (EU),
des Europäischen Wirtschaftsraums (EWR) oder der Schweizer Eidgenossenschaft)
verarbeite oder dies im Rahmen der Inanspruchnahme von Diensten Dritter oder
Offenlegung, bzw. Übermittlung von Daten an andere Personen oder Unternehmen
geschieht, erfolgt dies nur, wenn es zur Erfüllung meiner (vor)vertraglichen
Pflichten, auf Grundlage deiner Einwilligung, aufgrund einer rechtlichen
Verpflichtung oder auf Grundlage meiner berechtigten Interessen
geschieht.

Vorbehaltlich gesetzlicher oder vertraglicher Erlaubnisse, verarbeite
ich oder lasse ich die Daten in einem Drittland nur beim Vorliegen der
gesetzlichen Voraussetzungen verarbeiten. Das heißt, dass die Verarbeitung zum
Beispiel auf Grundlage besonderer Garantien, wie der offiziell anerkannten
Feststellung eines der EU entsprechenden Datenschutzniveaus (z.B. für die USA
durch das „Privacy Shield“) oder Beachtung offiziell anerkannter spezieller
vertraglicher Verpflichtungen erfolgt.

# Rechte der betroffenen Personen

Du hast das Recht, eine Bestätigung darüber zu verlangen, ob
betreffende Daten verarbeitet werden und auf Auskunft über diese Daten
sowie auf weitere Informationen und Kopie der Daten entsprechend den
gesetzlichen Vorgaben.

Du hast entsprechend den gesetzlichen Vorgaben das Recht, die
Vervollständigung der dich betreffenden Daten oder die Berichtigung der
dich betreffenden unrichtigen Daten zu verlangen.

Du hast nach Maßgabe der gesetzlichen Vorgaben das Recht zu verlangen, dass
betreffende Daten unverzüglich gelöscht oder für die weitere Verarbeitung
gesperrt werden.

Du hast das Recht zu verlangen, dass die dich betreffenden Daten, die
du mir bereitgestellt hast, nach Maßgabe der gesetzlichen Vorgaben zu
erhalten und deren Übermittlung an andere Verantwortliche zu fordern.

Du hast ferner nach Maßgabe der gesetzlichen Vorgaben das Recht, eine
Beschwerde bei der zuständigen Aufsichtsbehörde einzureichen.

# Widerrufsrecht

Du hast das Recht, erteilte Einwilligungen mit Wirkung für die Zukunft
zu widerrufen.

# Widerspruchsrecht

Du kannst der künftigen Verarbeitung der dich betreffenden Daten nach
Maßgabe der gesetzlichen Vorgaben jederzeit widersprechen. Der
Widerspruch kann insbesondere gegen die Verarbeitung für Zwecke der
Direktwerbung erfolgen.

# Cookies und Widerspruchsrecht bei Direktwerbung

Als „Cookies“ werden kleine Dateien bezeichnet, die auf Rechnern der
Nutzer gespeichert werden. Innerhalb der Cookies können unterschiedliche
Angaben gespeichert werden. Ein Cookie dient primär dazu, die Angaben zu
einem Nutzer (bzw. dem Gerät auf dem das Cookie gespeichert ist) während
oder auch nach seinem Besuch innerhalb eines Onlineangebotes zu
speichern.

Als temporäre Cookies, bzw. „Session-Cookies“ oder
„transiente Cookies“, werden Cookies bezeichnet, die gelöscht werden,
nachdem ein Nutzer ein Onlineangebot verlässt und seinen Browser
schließt. In einem solchen Cookie kann z.B. der Inhalt eines Warenkorbs
in einem Onlineshop oder ein Login-Status gespeichert werden. Als
„permanent“ oder „persistent“ werden Cookies bezeichnet, die auch nach
dem Schließen des Browsers gespeichert bleiben. So kann z.B. der
Login-Status gespeichert werden, wenn die Nutzer diese nach mehreren
Tagen aufsuchen. Ebenso können in einem solchen Cookie die Interessen
der Nutzer gespeichert werden, die für Reichweitenmessung oder
Marketingzwecke verwendet werden.

Als „Third-Party-Cookie“ werden Cookies bezeichnet, die von anderen Anbietern
als dem Verantwortlichen, der das Onlineangebot betreibt, angeboten werden
(andernfalls, wenn es nur dessen Cookies sind spricht man von „First-Party
Cookies“).

Ich kann temporäre und permanente Cookies einsetzen und kläre
hierüber im Rahmen meiner Datenschutzerklärung auf.

Falls die Nutzer nicht möchten, dass Cookies auf ihrem Rechner
gespeichert werden, werden sie gebeten die entsprechende Option in den
Systemeinstellungen ihres Browsers zu deaktivieren. Gespeicherte Cookies
können in den Systemeinstellungen des Browsers gelöscht werden. Der
Ausschluss von Cookies kann zu Funktionseinschränkungen dieses
Onlineangebotes führen.

Ein genereller Widerspruch gegen den Einsatz der zu Zwecken des
Onlinemarketing eingesetzten Cookies kann bei einer Vielzahl der
Dienste, vor allem im Fall des Trackings, über die US-amerikanische
Seite <http://www.aboutads.info/choices/> oder die EU-Seite
<http://www.youronlinechoices.com/> erklärt werden. Des Weiteren kann
die Speicherung von Cookies mittels deren Abschaltung in den
Einstellungen des Browsers erreicht werden. Bitte beachten Sie, dass
dann gegebenenfalls nicht alle Funktionen dieses Onlineangebotes genutzt
werden können.

# Löschung von Daten

Die von mir verarbeiteten Daten werden nach Maßgabe der gesetzlichen
Vorgaben gelöscht oder in ihrer Verarbeitung eingeschränkt. Sofern nicht
im Rahmen dieser Datenschutzerklärung ausdrücklich angegeben, werden die
bei mir gespeicherten Daten gelöscht, sobald sie für ihre
Zweckbestimmung nicht mehr erforderlich sind und der Löschung keine
gesetzlichen Aufbewahrungspflichten entgegenstehen.

Sofern die Daten nicht gelöscht werden, weil sie für andere und
gesetzlich zulässige Zwecke erforderlich sind, wird deren Verarbeitung
eingeschränkt. D.h. die Daten werden gesperrt und nicht für andere
Zwecke verarbeitet. Das gilt z.B. für Daten, die aus handels- oder
steuerrechtlichen Gründen aufbewahrt werden müssen.

# Änderungen und Aktualisierungen der Datenschutzerklärung

Ich bitte dich sich regelmäßig über den Inhalt meiner
Datenschutzerklärung zu informieren. Ich passe die Datenschutzerklärung
an, sobald die Änderungen der von mir durchgeführten Datenverarbeitungen
dies erforderlich machen. Ich informiere dich, sobald durch die
Änderungen eine Mitwirkungshandlung deinerseits (z.B. Einwilligung) oder
eine sonstige individuelle Benachrichtigung erforderlich wird.

# DISQUS-Kommentarfunktion

Ich setze auf Grundlage meiner berechtigten Interessen an einer
effizienten, sicheren und nutzerfreundlichen Kommentarverwaltung gem.
Art. 6 Abs. 1 lit. f. DSGVO den Kommentardienst DISQUS, angeboten von
der DISQUS, Inc., 301 Howard St, Floor 3 San Francisco, California-
94105, USA, ein. DISQUS ist unter dem Privacy-Shield-Abkommen
zertifiziert und bietet hierdurch eine Garantie, das europäische
Datenschutzrecht einzuhalten:
<https://www.privacyshield.gov/participant?id=a2zt0000000TRkEAAW&status=Active>.

Zur Nutzung der DISQUS Kommentarfunktion können Nutzer sich über ein
eigenes DISQUS-Nutzer-Konto oder einen bestehende Social-Media-Konten
(z.B. OpenID, Facebook, Twitter oder Google) anmelden. Hierbei werden
die Anmeldedaten der Nutzer durch DISQUS von den Plattformen bezogen. Es
ist ebenfalls möglich, die DISQUS-Kommentarfunktion als Gast, ohne
Erstellung oder Verwendung Nutzerkontos bei DISQUS oder einem der
angegebenen Social-Media-Anbieter, zu nutzen.

Ich bette lediglich DISQUS mit seinen Funktionen in meiner Website ein,
wobei ich auf die Kommentare der Nutzer Einfluss nehmen kann. Die
Nutzer treten jedoch in eine unmittelbare Vertragsbeziehung mit DISQUS,
in deren Rahmen DISQUS die Kommentare der Nutzer verarbeitet und ein
Ansprechpartner für etwaige Löschung der Daten der Nutzer ist. Ich
verweisen hierbei auf [die Datenschutzerklärung von DISQUS](
<https://help.disqus.com/terms-and-policies/disqus-privacy-policy>) und
weise die Nutzer ebenfalls darauf hin, dass sie davon ausgehen können,
dass DISQUS neben dem Kommentarinhalt auch deren IP-Adresse und den
Zeitpunkt des Kommentars speichert sowie Cookies auf den Rechnern der
Nutzer speichert und zur Darstellung von Werbung nutzen kann. Nutzer
können jedoch der Verarbeitung ihrer Daten zwecks Darstellung von
Anzeigen widersprechen:
[https://disqus.com/data-sharing-settings](https://disqus.com/data-sharing-settings/).

# Hosting und E-Mail-Versand

Die von uns in Anspruch genommenen Hosting-Leistungen dienen der
Zurverfügungstellung der folgenden Leistungen: Infrastruktur- und
Plattformdienstleistungen, Rechenkapazität, Speicherplatz und
Datenbankdienste, E-Mail-Versand, Sicherheitsleistungen sowie technische
Wartungsleistungen, die ich zum Zwecke des Betriebs dieses
Onlineangebotes einsetzen.

Hierbei verarbeite ich bzw. mein Hostinganbieter Bestandsdaten,
Kontaktdaten, Inhaltsdaten, Vertragsdaten, Nutzungsdaten, Meta- und
Kommunikationsdaten von Kunden, Interessenten und Besuchern dieses
Onlineangebotes auf Grundlage meiner berechtigten Interessen an einer
effizienten und sicheren Zurverfügungstellung dieses Onlineangebotes
gem. Art. 6 Abs. 1 lit. f DSGVO i.V.m. Art. 28 DSGVO (Abschluss
Auftragsverarbeitungsvertrag).

# Erhebung von Zugriffsdaten und Logfiles

Ich, bzw. mein Hostinganbieter, erhebt auf Grundlage meiner
berechtigten Interessen im Sinne des Art. 6 Abs. 1 lit. f. DSGVO Daten
über jeden Zugriff auf den Server, auf dem sich dieser Dienst befindet
(sogenannte Serverlogfiles). Zu den Zugriffsdaten gehören Name der
abgerufenen Webseite, Datei, Datum und Uhrzeit des Abrufs, übertragene
Datenmenge, Meldung über erfolgreichen Abruf, Browsertyp nebst Version,
das Betriebssystem des Nutzers, Referrer URL (die zuvor besuchte Seite),
IP-Adresse und der anfragende Provider.

Logfile-Informationen werden aus Sicherheitsgründen (z.B. zur Aufklärung
von Missbrauchs- oder Betrugshandlungen) für die Dauer von maximal 7
Tagen gespeichert und danach gelöscht. Daten, deren weitere Aufbewahrung
zu Beweiszwecken erforderlich ist, sind bis zur endgültigen Klärung des
jeweiligen Vorfalls von der Löschung ausgenommen.

# Content-Delivery-Network von Cloudflare

Ich setze ein so genanntes "Content Delivery Network" (CDN), angeboten
von Cloudflare, Inc., 101 Townsend St, San Francisco, CA 94107, USA,
ein. Cloudflare ist unter dem Privacy-Shield-Abkommen zertifiziert und
bietet hierdurch eine Garantie, das europäische Datenschutzrecht
einzuhalten
(<https://www.privacyshield.gov/participant?id=a2zt0000000GnZKAA0&status=Active>).

Ein CDN ist ein Dienst, mit dessen Hilfe Inhalte meines
Onlineangebotes, insbesondere große Mediendateien, wie Grafiken oder
Skripte mit Hilfe regional verteilter und über das Internet verbundener
Server, schneller ausgeliefert werden. Die Verarbeitung der Daten der
Nutzer erfolgt alleine zu den vorgenannten Zwecken und der
Aufrechterhaltung der Sicherheit und Funktionsfähigkeit des CDN.

Die Nutzung erfolgt auf Grundlage meiner berechtigten Interessen, d.h.
Interesse an einer sicheren und effizienten Bereitstellung, Analyse
sowie Optimierung meines Onlineangebotes gem. Art. 6 Abs. 1 lit. f.
DSGVO.

Weitere Informationen erhältst du in der Datenschutzerklärung von
Cloudflare: <https://www.cloudflare.com/security-policy>.


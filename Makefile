info:
	@echo "Usage info"

	@echo "  make test              Testlauf mit Drafts"
	@echo "  make deploy            Seiten ohne Drafts generieren und ausliefern"

.PHONY:  deploy test

test:
	hugo server --disableFastRender -D -d dev

deploy:
	hugo
	scp -r public/*  ssh-w0181e22@emacs-lernen.de:/www/htdocs/w0181e22/emacs-lernen.de/
